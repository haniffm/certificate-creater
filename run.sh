#!/bin/sh -e

# Setup Let's encrypt api key/token
echo "dns_digitalocean_token = ${DIGITALOCEAN_TOKEN}" > dnsapikey
chmod 600 dnsapikey

DOMAIN_ARR=""
for i in $DOMAINS; do
        DOMAIN_ARR="${DOMAIN_ARR} -d ${i}"
done

certbot certonly --non-interactive --keep-until-expiring --expand \
        --email "${EMAIL}" --agree-tos \
        --config-dir "/data/cert" --work-dir "/data/workdir" \
        --cert-name "${CERT_NAME}" \
        --preferred-challenges dns ${DOMAIN_ARR} "--${PROVIDER}" "--${PROVIDER}-credentials" dnsapikey ${EXTRA_ARGS}

# Remove the api key
rm dnsapikey

