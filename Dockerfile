FROM python:3.8.2-alpine

ENV CERTBOT_VERSION 1.3.0

RUN apk add --no-cache --update \
		libffi \
		musl \
		openssl \
	&& apk add --no-cache --virtual .build-dependencies \
		g++ \
		libffi-dev \
		musl-dev \
		openssl-dev \
	&& pip3 install --no-cache-dir \
		certbot==${CERTBOT_VERSION} \
		certbot-dns-digitalocean==${CERTBOT_VERSION} \
	&& apk del .build-dependencies

COPY run.sh .

CMD ["./run.sh"]
